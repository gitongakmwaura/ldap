<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class LdapController extends Controller
{

    # Authentication
    public function ldapAuthentication()
    {



        // LDAP server details : 10.200.4.100
        $ldapHost = 'ldap://10.200.4.100';
        $ldapPort = 389; #or 389 based on SSL 
        // $ldapPort = 636; #or 389 based on SSL 

        // LDAP server credentials
        $ldapUsername = 'court\\fortinet.rdp';
        // $ldapUsername = 'court\\selfreset';
        // $ldapUsername = 'court\\fortinet';
        // $ldapPassword = 'Reset@2030'; // Replace with your LDAP server
        $ldapPassword = 'Jud@3030'; // Replace with your LDAP server
        // $ldapPassword = 'Kenya@2030'; // Replace with your LDAP server
        // $ldapPassword = 'Qwerty65432!'; // Replace with your LDAP server


        // Connect to the LDAP server
        $ldapConnection = ldap_connect($ldapHost, $ldapPort);
        // ldap_start_tls($ldapConnection);

        if (!$ldapConnection) {
            die("LDAP connection failed.");
        }

        // Set LDAP options
        ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);

        ldap_set_option($ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3);
        // ldap_set_option($ldapConnection, LDAP_OPT_X_TLS_CACERTDIR, '/');
        // ldap_set_option($ldapConnection, LDAP_OPT_X_TLS_CACERTFILE, '/STAR_court_go_ke.crt');
        ldap_set_option($ldapConnection, LDAP_OPT_X_TLS_CACERTDIR, '/');
        ldap_set_option($ldapConnection, LDAP_OPT_X_TLS_CACERTFILE, '/courtldapsssl.pem');
        // dd($ldapConnection);

        // Bind to the LDAP server with credentials
        $ldapBind = ldap_bind($ldapConnection, $ldapUsername, $ldapPassword);

        // dd($ldapBind);
        if (!$ldapBind) {
            return "LDAP bind failed. Check your credentials.";
        } else {

            // dd("connected  bound");
            return $ldapConnection;
        }

    }

    public function ldapAuth()
    {

        // ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);
        // $ldapconn = ldap_connect('ldaps://prprodad1.court.go.ke', 636);
        // $ldapconn = ldap_connect('ldaps://10.200.4.110', 636);
        // var_dump(openssl_get_cert_locations());

        $ldapconn = ldap_connect('ldaps://10.200.4.100', 636);
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapconn, LDAP_OPT_X_TLS_REQUIRE_CERT, 0);

        // ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 1);
        // ldap_set_option($ldapconn, LDAP_OPT_X_TLS_CACERTFILE, '/Users/jy/Projects/JY/ldap/my-root-ca-cert.cer');

        // ldap_set_option($ldapconn, LDAP_OPT_X_TLS_REQUIRE_CERT, LDAP_OPT_X_TLS_DEMAND);
        // ldap_set_option($ldapconn, LDAP_OPT_X_TLS_CACERTDIR, '/Users/jy/Projects/JY/ldap');
        // ldap_set_option($ldapconn, LDAP_OPT_X_TLS_CACERTFILE, '/Users/jy/Projects/JY/ldap/courtldapsssl.pem');

        // flexi_auth
        // Auth.php controller
        // dd($ldapconn);
        // $ldapuser = "court\\fortinet.rdp";
        // $ldapuser = "MAHAKAMA1\mwaura";
        $ldapuser = "court\administrator";
        $ldappwd = "Jumuika@2023!";

        // $ldappwd = "Jud@2030";
        // $ldappwd = "Jud@3030";
        // $ldappwd = "Welcome.1234";
        $username = "79211";
        $oldpassword = "Admin@2022";
        $newpassword = "Admin@20224";
        $newpassword2 = "Admin@20224";

        // search for user
        $status = ldap_bind($ldapconn, $ldapuser, $ldappwd);
        dd($status);
        $res_id = ldap_search($ldapconn, "DC=court,DC=go,DC=ke", "sAMAccountName=$username");
        // dd($res_id);
        // $res_id = ldap_search($ldapconn, "CN=JUDICIARY-ADMIN,DC=court,DC=g,DC=ke", "sAMAccountName=$username");
        if ($res_id) {
            $entry_id = ldap_first_entry($ldapconn, $res_id);
            if ($entry_id) {
                $user_dn = ldap_get_dn($ldapconn, $entry_id);
                if ($user_dn) {
                    $ldapbind = ldap_bind($ldapconn, $user_dn, $oldpassword);
                    // check if the old password allows a successfull login
                    if ($ldapbind) {
                        if (strcmp($newpassword, $newpassword2) == 0) {

                            // create the unicode password
                            $newpassword = "\"" . $newpassword . "\"";
                            $newPass = mb_convert_encoding($newpassword, "UTF-16LE");

                            //rebind as admin to change the password
                            ldap_bind($ldapconn, $ldapuser, $ldappwd);

                            $pwdarr = array('unicodePwd' => $newPass);
                            if (ldap_mod_replace($ldapconn, $user_dn, $pwdarr)) {
                                print "<p class='success'>Change password succeded.</p>\n";
                            } else {
                                print "<p class='error'>Change password failed.</p>\n";
                            }
                        } else {
                            print "<p class='error'>New password must be entered the same way twice.</p>\n";
                        }
                    } else {
                        print "<p class='error'>Wrong user name or password.</p>\n";
                    }
                } else {
                    print "<p class='error'>Couldn't load user data.</p>\n";
                }
            } else {
                print "<p class='error'>Couldn't find user data.</p>\n";
            }
        } else {
            print "<p class='error'>Username was not found.</p>\n";
        }
        if (ldap_error($ldapconn) != "Success") {
            print "<p class='error'>LDAP Error:<br />\n";
            var_dump(ldap_error($ldapconn));
            print "</p>\n";
        }
        @ldap_close($ldapconn);


    }
    public function ldapSearch($ldapConnection, $searchFilter)
    {
        // Search settings
        // $baseDN = 'CN=SSLVPN,OU=VPN_ACCESS,DC=court,DC=go,DC=ke';
        $baseDN = 'OU=VPN_ACCESS,DC=court,DC=go,DC=ke';
        // $baseDN = 'dc=court,dc=go,dc=ke';
        $searchFilter = $searchFilter;
        // Perform the LDAP search
        $searchResults = ldap_search($ldapConnection, $baseDN, $searchFilter);
        if ($searchResults === false) {
            die("LDAP search failed.");
        }

        // Get the first entry from the search result
        $entry = ldap_first_entry($ldapConnection, $searchResults);

        if ($entry !== false) {
            // Retrieve attributes from the entry
            $attributes = ldap_get_attributes($ldapConnection, $entry);
            return $attributes;

        } else {

            return "User not found.";
        }

        // Close the LDAP connection when done
        ldap_unbind($ldapConnection);
    }
    public function ldapSearchPaginated($ldapConnection, $searchFilter)
    {
        $baseDN = 'OU=VPN_ACCESS,DC=court,DC=go,DC=ke';
        $cookie = '';

        $users = [];

        do {
            $result = ldap_search(
                $ldapConnection,
                $baseDN,
                $searchFilter,
                ['cn'],
                0,
                0,
                0,
                LDAP_DEREF_NEVER,
                [['oid' => LDAP_CONTROL_PAGEDRESULTS, 'value' => ['size' => 750, 'cookie' => $cookie]]]
            );

            ldap_parse_result($ldapConnection, $result, $errcode, $matcheddn, $errmsg, $referrals, $controls);

            $entries = ldap_get_entries($ldapConnection, $result);

            foreach ($entries as $entry) {
                if (isset($entry['cn'][0])) {
                    $users[] = $entry['cn'][0];
                }
            }

            if (isset($controls[LDAP_CONTROL_PAGEDRESULTS]['value']['cookie'])) {
                $cookie = $controls[LDAP_CONTROL_PAGEDRESULTS]['value']['cookie'];
            } else {
                $cookie = '';
            }
        } while (!empty($cookie));

        return $users;
    }

}

