<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show($pj_number)
    {
        // Fetch User Info from AD via LDAP
        $ldap = new LdapController();
        $ldapConnection = $ldap->ldapAuthentication();
        $searchUser = $ldap->ldapSearch($ldapConnection, '(sAMAccountName=' . (int) $pj_number . ')');
        $cn = $searchUser["cn"][0];
        $sn = $searchUser["sn"][0];
        $givenName = $searchUser["givenName"][0];
        $distinguishedName = $searchUser["distinguishedName"][0];
        $whenCreated = $searchUser["whenCreated"][0];
        $whenChanged = $searchUser["whenChanged"][0];
        $displayName = $searchUser["displayName"][0];
        $uSNCreated = $searchUser["uSNCreated"][0];
        $uSNChanged = $searchUser["uSNChanged"][0];
        $objectGUID = $searchUser["objectGUID"][0]; # has invalid UTF - 8 characters
        $userAccountControl = $searchUser["userAccountControl"][0];
        $badPwdCount = $searchUser["badPwdCount"][0];
        $codePage = $searchUser["codePage"][0];
        $countryCode = $searchUser["countryCode"][0];
        $badPasswordTime = $searchUser["badPasswordTime"][0];
        $lastLogoff = $searchUser["lastLogoff"][0];
        $lastLogon = $searchUser["lastLogon"][0];
        $pwdLastSet = $searchUser["pwdLastSet"][0];
        $primaryGroupID = $searchUser["primaryGroupID"][0];
        $objectSid = $searchUser["objectSid"][0];   # has invalid UTF - 8 characters
        $accountExpires = $searchUser["accountExpires"][0];
        $logonCount = $searchUser["logonCount"][0];
        $sAMAccountName = $searchUser["sAMAccountName"][0];
        $sAMAccountType = $searchUser["sAMAccountType"][0];
        $userPrincipalName = $searchUser["userPrincipalName"][0];
        $objectCategory = $searchUser["objectCategory"][0];
        $dSCorePropagationData = $searchUser["dSCorePropagationData"][0];
        $lastLogonTimestamp = $searchUser["lastLogonTimestamp"][0];

        // Return JSON response
        return response()->json([
            'pj_number' => $pj_number,
            'cn' => $cn,
            'sn' => $sn,
            'givenName' => $givenName,
            'distinguishedName' => $distinguishedName,
            'whenCreated' => $whenCreated,
            'whenChanged' => $whenChanged,
            'displayName' => $displayName,
            'uSNCreated' => $uSNCreated,
            'uSNChanged' => $uSNChanged,
            'userAccountControl' => $userAccountControl,
            'badPwdCount' => $badPwdCount,
            'codePage' => $codePage,
            'countryCode' => $countryCode,
            'badPasswordTime' => $badPasswordTime,
            'lastLogoff' => $lastLogoff,
            'lastLogon' => $lastLogon,
            'pwdLastSet' => $pwdLastSet,
            'primaryGroupID' => $primaryGroupID,
            'accountExpires' => $accountExpires,
            'logonCount' => $logonCount,
            'sAMAccountName' => $sAMAccountName,
            'sAMAccountType' => $sAMAccountType,
            'userPrincipalName' => $userPrincipalName,
            'objectCategory' => $objectCategory,
            'dSCorePropagationData' => $dSCorePropagationData,
            'lastLogonTimestamp' => $lastLogonTimestamp,

        ]);
    }

    public function getAllADUsers()
    {
        $ldap = new LdapController();
        $ldapConnection = $ldap->ldapAuthentication();
        // $searchUsers = $ldap->ldapSearchPaginated($ldapConnection, '(ou=VPN_ACCESS)');
        $searchUsers = $ldap->ldapSearchPaginated($ldapConnection, '(sAMAccountName=*)');
        // $searchUsers = $ldap->ldapSearchPaginated($ldapConnection, '(&(objectClass=user)(objectCategory=person)(OU=VPN_ACCESS))');
        // $searchUsers = $ldap->ldapSearchPaginated($ldapConnection, '(|(objectCategory=person)(objectCategory=contact))');
        // var_dump($searchUsers);

        // $cleanResults = [];
        // foreach ($searchUsers as $key => $value) {
        //     if (is_int($key)) {
        //         $cleanResults[] = $value;
        //     }
        // }

        // $users = [];

        // foreach ($cleanResults as $key => $value) {
        //     $cn = $value['cn'][0];
        //     $dn = $value['dn'];


        //     // Create a subarray to store user attributes
        //     $userAttributes = [
        //         'cn' => $cn,
        //         'dn' => $dn,
        //         '0' => $value[0],
        //     ];

        //     // Push the subarray to the $users array
        //     $users[] = $userAttributes;
        // }

        // return json
        return response()->json([
            'users' => $searchUsers,
            'count' => count($searchUsers),
        ]);
    }

    public function resetPassword(Request $request)
    {

        Log::info("request");
        Log::info($request);
        // dd($request);
        // get user info from request
        $pj_number = (int) $request->pjNumber;
        $newPassword = $request->newPassword;
        $confirmPassword = $request->confirmPassword;

        // check if passwords match
        if ($newPassword != $confirmPassword) {
            return response()->json([
                'message' => 'Passwords do not match.',
                'status' => 'failed',
            ]);
        } else {
            // check if password is empty
            if (empty ($newPassword)) {
                return response()->json([
                    'message' => 'Password cannot be empty.',
                    'status' => 'failed',
                ]);
            }
        }

        // connect to AD via LDAP
        $ldap = new LdapController();
        $ldapConnection = $ldap->ldapAuthentication();

        // search for user using pj_number
        $searchUser = $ldap->ldapSearch($ldapConnection, '(sAMAccountName=' . $pj_number . ')');
        if ($searchUser == "User not found.") {
            return response()->json([
                'message' => 'User not found.',
                'status' => 'failed',
            ]);
        }
        //  dd($searchUser);
        // get user attributes if user exists
        $cn = $searchUser["cn"][0];
        $sn = $searchUser["sn"][0];
        $givenName = $searchUser["givenName"][0];
        $distinguishedName = $searchUser["distinguishedName"][0];


        // get password related attributes
        // $oldPassword = $searchUser["userPassword"][0];
        $userAccountControl = $searchUser["userAccountControl"][0];
        $badPwdCount = $searchUser["badPwdCount"][0];
        $badPasswordTime = $searchUser["badPasswordTime"][0];
        $lastLogon = $searchUser["lastLogon"][0];
        $pwdLastSet = $searchUser["pwdLastSet"][0];
        $accountExpires = $searchUser["accountExpires"][0];
        $logonCount = $searchUser["logonCount"][0];
        $sAMAccountName = $searchUser["sAMAccountName"][0];
        $sAMAccountType = $searchUser["sAMAccountType"][0];
        // echo  $oldPassword . "<br>";
        // echo "new" . "{MD5}" . base64_encode(( md5($newPassword))) . "<br>";
        $oldHash = "{MD5}p7Z6V9vuVSvFjUPVlb5yIg==";
        $newHash = "{MD5}" . base64_encode(pack("H*", md5($newPassword)));

        // $decodedOldHash = base64_decode(substr($oldPassword, 5));
        $decodedNewHash = base64_decode(substr($newHash, 5));
        //  echo $oldPassword . "<br>";
        // //echo $newHash . "<br>";
        // if ($decodedOldHash === $decodedNewHash) {
        //     return response()->json([

        //         'message' => 'The old and new hashes match. They correspond to the same password.',
        //         'status' => 'failed',
        //     ]);
        // }


        //         Import-Module ActiveDirectory

        // $users = Get-Content C:\Users\fortinet.rdp\Documents\Reset_bulk_AD.csv

        // foreach ($user in $users) {


        //     $RandomNumber = Get-Random -Minimum 2000 -Maximum 2101
//     $Password = "Jud@" + $RandomNumber.ToString()

        //     $NewPwd = ConvertTo-SecureString $Password -AsPlainText -Force

        //     Set-ADAccountPassword $user -NewPassword $NewPwd -Reset

        //     Set-ADUser -Identity $user -ChangePasswordAtLogon $true


        //     Write-Host $user, $Password


        // }

        // Remove leading/trailing whitespace
        $newPassword = trim($newPassword);

        // Requirements
        // length 8
        // Special characters
        // No name
        // No PJ
        // Caps & Small Letters
        // Check password length
        if (strlen($newPassword) < 8) {
            return response()->json([
                'message' => 'Password must be exactly 8 characters long.',
                'status' => 'failed',
            ]);
        }

        // Check for special characters
        if (!preg_match('/[!@#$%^&*()_+{}\[\]:;<>,.?~\\-=\|]/', $newPassword)) {
            return response()->json([
                'message' => 'Password must contain at least one special character.',
                'status' => 'failed',
            ]);
        }

        // Check for uppercase and lowercase letters
        if (!preg_match('/[A-Z]/', $newPassword) || !preg_match('/[a-z]/', $newPassword)) {
            return response()->json([
                'message' => 'Password must contain both uppercase and lowercase letters.',
                'status' => 'failed',
            ]);
        }
        // check if old password hash is matching to the new password hash
        // if ($oldPassword == "{MD5}" . base64_encode(pack("H*", md5($newPassword)))) {
        //     return response()->json([
        //         'message' => 'New password cannot be the same as the old password.',
        //         'status' => 'failed',
        //     ]);
        // }
        // Check if the user's name appears in the new password
        if (stripos($newPassword, $cn) !== false) {
            return response()->json([
                'message' => 'Password cannot contain your name.',
                'status' => 'failed',
            ]);
        }
        // Check if the PJ number appears in the new password
        if (
            stripos($newPassword, $sAMAccountName) !== false
        ) {
            return response()->json([
                'message' => 'Password cannot contain your PJ number.',
                'status' => 'failed',
            ]);
        }

        $newpassword = "\"" . $newPassword . "\"";
        $newPass = mb_convert_encoding($newpassword, "UTF-16LE");
        $pwdarr = array('unicodePwd' => $newPass);
        // $pwdarr = array('givenName' => "Mitei");
        ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, 7);
        $ldapconn = ldap_connect('ldap://10.200.4.100', 389);
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
        // Disable SSL certificate verification (for testing only)
        // ldap_set_option($ldapconn, LDAP_OPT_X_TLS_REQUIRE_CERT, LDAP_OPT_X_TLS_NEVER);

        ldap_set_option($ldapconn, LDAP_OPT_X_TLS_CACERTDIR, '/');
        ldap_set_option($ldapconn, LDAP_OPT_X_TLS_CACERTFILE, '/cert.pem');

        // var_dump($ldapconn);

        $ldapuser = "fortinet.rdp";
        $ldappwd = "Jud@3030";
        $ldapUsername = 'court\\fortinet.rdp';
        // $ldapUsername = 'court\\fortinet';
        // $ldapPassword = 'Reset@2030'; // Replace with your LDAP server
        $ldapPassword = 'Jud@3030';
        // $bind_status = ldap_bind($ldapconn, "CN=$ldapuser, DC=court,DC=go,DC=ke", $ldappwd);
        $ldapBind = ldap_bind($ldapconn, $ldapUsername, $ldapPassword);

        //rebind as admin to change the password
        // // $bind_status = ldap_bind($ldapconn, "CN='court\\fortinet.rdp',CN=SSLVPN,DC=court,DC=go,DC=ke", 'Welcome.1234');

        // dd($ldapBind);
        // var_dump($pwdarr);
        // var_dump($ldapconn);
        // var_dump($ldapConnection);
        // var_dump($distinguishedName);
        // return json response
        $status = ldap_mod_replace($ldapconn, $distinguishedName, $pwdarr);
        // update user password
        // $newEntry = array('userPassword' => "{MD5}" . base64_encode(pack("H*", md5($newPassword))));
        $status = ldap_mod_replace($ldapConnection, $distinguishedName, $pwdarr);
        // var_dump($status);
        if ($status) {
            return response()->json([
                'message' => 'Password reset for ' . $cn . ' was successful',
                'status' => 'success',
                'response' => $status,
            ]);
        } else {
            return response()->json([
                'message' => 'Password reset for ' . $cn . ' failed',
                'status' => $status,
            ]);
        }

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        //
    }
}
