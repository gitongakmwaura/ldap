<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// LDAP Login
Route::get('/ldap', 'App\Http\Controllers\LdapController@authentication');

// Get User Info from AD using PJ Number
Route::get('/user/{pj_number}', 'App\Http\Controllers\UserController@show');

// Reset Password for a User
Route::post('/user/reset-password', 'App\Http\Controllers\UserController@resetPassword');