<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// LDAP Login
Route::get('/ldap', 'App\Http\Controllers\LdapController@authentication');
Route::get('/ldap/auth', 'App\Http\Controllers\LdapController@ldapAuth');


// Get User Info from AD using PJ Number
Route::get('/user/{pj_number}', 'App\Http\Controllers\UserController@show');

//GET All Users
Route::get('/users', 'App\Http\Controllers\UserController@getAllADUsers');

// Reset Password for a User
Route::post('/user/reset-password', 'App\Http\Controllers\UserController@resetPassword');